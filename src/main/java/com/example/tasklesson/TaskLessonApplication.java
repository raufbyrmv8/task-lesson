package com.example.tasklesson;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskLessonApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskLessonApplication.class, args);
    }

}
