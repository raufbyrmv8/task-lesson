package com.example.tasklesson.dto;

import com.example.tasklesson.enums.Category;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Table;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductsDto {
    String name;
    Integer price;
    @Enumerated(EnumType.STRING)
    Category category;
    String description;
}
