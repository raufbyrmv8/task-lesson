package com.example.tasklesson.repository;

import com.example.tasklesson.dto.CategoryCountDto;
import com.example.tasklesson.model.Products;
import com.example.tasklesson.service.CategoryCount;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public interface ProductsRepository extends JpaRepository<Products,Long> {
    @Query("SELECT p FROM Products p WHERE (:priceFrom IS NULL OR p.price >= :priceFrom) AND (:priceTo IS NULL OR p.price<= :priceTo)")
    List<Products> findAllByPrice(@Param("priceFrom") Integer priceFrom,@Param("priceTo") Integer priceTo);

    @Query("SELECT p.category AS category, COUNT(p) AS count from Products p GROUP BY p.category")
    List<CategoryCount> countProductsByCategory();
}
