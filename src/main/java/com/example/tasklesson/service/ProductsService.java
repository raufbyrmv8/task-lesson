package com.example.tasklesson.service;

import com.example.tasklesson.dto.CategoryCountDto;
import com.example.tasklesson.dto.ProductsDto;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ProductsService {
    ProductsDto createProduct(ProductsDto productsDto);
    List<ProductsDto> findAllByPrice(@Param("priceFrom") Integer priceFrom, @Param("priceTo") Integer priceTo);
    List<CategoryCountDto> countProductsByCategory();
}
