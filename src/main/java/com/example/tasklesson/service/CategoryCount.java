package com.example.tasklesson.service;

public interface CategoryCount {
    String getCategory();
    long getCount();
}
