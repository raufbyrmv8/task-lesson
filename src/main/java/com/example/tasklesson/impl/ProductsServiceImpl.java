package com.example.tasklesson.impl;

import com.example.tasklesson.dto.CategoryCountDto;
import com.example.tasklesson.dto.ProductsDto;
import com.example.tasklesson.mapper.ProductMapper;
import com.example.tasklesson.model.Products;
import com.example.tasklesson.repository.ProductsRepository;
import com.example.tasklesson.service.ProductsService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductsServiceImpl implements ProductsService {
    private final ProductsRepository productsRepository;
    private final ModelMapper modelMapper;
    private final ProductMapper productMapper;
    @Override
    public ProductsDto createProduct(ProductsDto productsDto) {
      Products products = productMapper.INSTANCE.productsDtoToProducts(productsDto);
      return productMapper.INSTANCE.productsToProductsDto(productsRepository.save(products));
    }

    @Override
    public List<ProductsDto> findAllByPrice(@Param("priceFrom") Integer priceFrom, @Param("priceTo") Integer priceTo) {
        List<Products>findAll = productsRepository.findAll();
       return findAll.stream()
               .map(products -> modelMapper.map(products, ProductsDto.class))
               .collect(Collectors.toList());

    }
    @Override
    public List<CategoryCountDto> countProductsByCategory() {
        return productsRepository.countProductsByCategory().stream()
                .map(projection -> new CategoryCountDto(projection.getCategory(), projection.getCount()))
                .collect(Collectors.toList());
    }


}
