package com.example.tasklesson.mapper;

import com.example.tasklesson.dto.ProductsDto;
import com.example.tasklesson.model.Products;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Component;

import static org.mapstruct.ReportingPolicy.IGNORE;

@Mapper(componentModel = "spring",unmappedTargetPolicy = IGNORE)
@Component
public interface ProductMapper {
    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    ProductsDto productsToProductsDto(Products products);
    @InheritInverseConfiguration
    Products productsDtoToProducts(ProductsDto productsDto);
}
