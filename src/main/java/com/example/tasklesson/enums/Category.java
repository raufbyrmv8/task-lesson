package com.example.tasklesson.enums;

public enum Category {
    KITAB,
    ELEKTRONIKA,
    MEISET
}