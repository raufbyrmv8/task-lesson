package com.example.tasklesson.controller;

import com.example.tasklesson.dto.CategoryCountDto;
import com.example.tasklesson.dto.ProductsDto;
import com.example.tasklesson.enums.Category;
import com.example.tasklesson.model.Products;
import com.example.tasklesson.repository.ProductsRepository;
import com.example.tasklesson.service.CategoryCount;
import com.example.tasklesson.service.ProductsService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping("/products")
public class ProductsController {
    private final ProductsService productsService;
    @PostMapping("/save")
    public ProductsDto save(@RequestBody ProductsDto productsDto){
        return productsService.createProduct(productsDto);
    }
    @GetMapping("find-all")
    public List<ProductsDto> getProducts(
            @RequestParam(required = false) Integer priceFrom,
            @RequestParam(required = false) Integer priceTo) {
        return productsService.findAllByPrice(priceFrom, priceTo);
    }

    @GetMapping("/count-by-category")
    public List<CategoryCountDto> countProductsByCategory() {
        return productsService.countProductsByCategory();
    }
}
